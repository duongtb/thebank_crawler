/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import collector.Collector;
import java.util.LinkedList;
import updater.IUpdater;

/**
 *
 * @author root
 */
public class Dispatcher {
    public static int BreakingInterval;
    public void setBreakingInterval( int breakingInterval ){
        BreakingInterval = breakingInterval;
    }
    
    public static int UpdaterDelay;
    public void setUpdaterDelay( int updaterDelay ){
        UpdaterDelay = updaterDelay;
    }
    
    public static void main( String[] args ) {
        LinkedList<Collector> llCol = Core.Collectors;
        
        while( true ){
            for( int i = 0; i < llCol.size(); i++ ){
                Collector co = llCol.get( i );
                
                String msg = "Collector " + co.getClass() + " is starting";
                System.out.println( msg );
                
                Thread t = new Thread( co );
                t.start();
            }
            
            try{
                Thread.sleep( UpdaterDelay );
            } catch( Exception ex ) {
                ex.printStackTrace();
            }
            
            String msg = "Starting Uploading progress";
            System.out.println( msg );
            IUpdater up = Core.getUpdater();
            up.update();
            
            try{
                Thread.sleep( BreakingInterval );
            }catch( Exception ex ) {
                ex.printStackTrace();
            }
        }
    }
 }
