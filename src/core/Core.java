/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package core;

import collector.Collector;
import collector.ICollectorListener;
import dao.IDB;
import dao.impl.DB;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import updater.IUpdater;

/**
 *
 * @author root
 */
public abstract class Core {
    static final String BeansFile = "Beans.xml";
    static ApplicationContext context = new ClassPathXmlApplicationContext( BeansFile );
    
    public static IDB DB;
    static{
        try{
            Map idbMap = context.getBeansOfType( IDB.class );
            Collection ll = idbMap.values();
            Object[] objs = ll.toArray();
            DB = (IDB) objs[0];
            
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }
    
    static LinkedList<Collector> Collectors = new LinkedList<Collector>();
    static{
        try{
            Map collectors = context.getBeansOfType( Collector.class );
            Collection coco = collectors.values();
            Object[] coArr = coco.toArray();
            for( int i = 0; i < coArr.length; i++ ){
                Collectors.add( (Collector) coArr[i] );
            }
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }
    
    static LinkedList<ICollectorListener> CollectorListeners = new LinkedList<ICollectorListener>();
    static{
        try{
            Map lisMap = context.getBeansOfType( ICollectorListener.class );
            Collection coco = lisMap.values();
            Object[] coArr = coco.toArray();
            for( int i = 0; i < coArr.length; i++ ){
                CollectorListeners.add( (ICollectorListener) coArr[i] );
            }
            
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }
    
    static{
        Collector.setListeners( CollectorListeners );
    }
    
    public static IUpdater Updater;
    static{
        try{
            Map map = context.getBeansOfType( IUpdater.class );
            Collection co = map.values();
            Object[] objs = co.toArray();
            Updater = (IUpdater) objs[0];
        } catch( Exception ex ) {
            String msg = "Error in Init IUpdater";
            System.out.println( msg );
            ex.printStackTrace();
        }
    }
    public static IUpdater getUpdater(){
        return Updater;
    }
    
    public static void main( String[] args ) {
        System.out.println( "Done" );
        Core.DB.save( null );
    }
}
