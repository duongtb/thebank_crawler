/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import dao.crawlerdb.pojos.Posts;

/**
 *
 * @author root
 */
public interface IDB {
    
    /**
     * Save a object to DB.
     * This method already check the existant.
     * @param post
     * @return 
     *      0: OK
     *      1: The specified post is ALREADY EXISTED.
     *      2: Object type not supported.
     *      3: null
     */
    public int save( Object obj );
    
    
    
    /**
     * Check the existant base on Content column.
     * @param obj
     * @return 
     */
    public boolean isExist( Object obj );
    
    /**
     * Update Content and other field. Identifier is TITLE field.
     * @param obj
     * @return 
     *          0: OK
     *          1: Error in updating.
     */
    public int update( Object obj );
    
    /**
     * Utilities for check a updating of a post. CrawlDB
     * This method only support for CrawlDB
     * @param postTitle
     * @return 
     */
    public String getContent( String postTitle );
    
}
