/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.crawlerdb;

import dao.crawlerdb.pojos.Constant;
import dao.crawlerdb.pojos.Posts;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author root
 */
public class CrawlerHU {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
//            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
            String xmlPath = "dao/crawlerdb/crawler.hibernate.cfg.xml";
            Configuration config = new Configuration().configure( xmlPath );
            sessionFactory = config.buildSessionFactory();
        }catch( Throwable ex ) {
            // Log the exception. 
            System.err.println( "Initial SessionFactory creation failed." + ex );
            throw new ExceptionInInitializerError( ex );
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static boolean isExist( Posts post ){
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        Criteria crit = session.createCriteria( Posts.class );
        crit.add( Restrictions.eq( Constant.Posts_crawlUrl_columnName, post.getCrawlUrl() ) );
        List l = crit.list();
        
        session.close();
        
        if( l.isEmpty() ){
            return false;
        }
        return true;
    }

    public static int save( Posts posts ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();        
        session.save( posts );
        session.close();
        return 0;
    }

    public static int update( Posts posts ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        String crawlUrl = posts.getCrawlUrl();
        
        Criteria crit = session.createCriteria( Posts.class );
        crit.add( Restrictions.eq( Constant.Posts_crawlUrl_columnName, crawlUrl ) );
        List l = crit.list();
        for( int i = 0; i < l.size(); i++ ){
            Posts p = (Posts) l.get( i );
            copyValue( posts, p );
//            System.out.println( "p.content = " + p.getContent() );
            session.update( p );
            
        }
        session.flush();
        session.close();
        return 0;
    }
    
    private static void copyValue( Posts source, Posts destination ){
        if( source == null || destination == null ){
            return;
        }
        destination.setAuthor( source.getAuthor() );
        destination.setCommentCount( source.getCommentCount() );
        destination.setCommentStatus( source.getCommentStatus() );
        destination.setContent( source.getContent() );
        destination.setDate( source.getDate() );
        destination.setDateGmt( source.getDateGmt() );
        destination.setExcerpt( source.getExcerpt() );
        destination.setGuid( source.getGuid() );
        destination.setMenuOrder( source.getMenuOrder() );
        destination.setPingStatus( source.getPingStatus() );
        destination.setPostContentFiltered( source.getPostContentFiltered() );
        destination.setPostMimeType( source.getPostMimeType() );
        destination.setPostModified( source.getPostModified() );
        destination.setPostModifiedGmt( source.getPostModifiedGmt() );
        destination.setPostName( source.getPostName() );
        destination.setPostParent( source.getPostParent() );
        destination.setPostPassword( source.getPostPassword() );
        destination.setPostType( source.getPostType() );
        destination.setStatus( source.getStatus() );
        destination.setTitle( source.getTitle() );
        destination.setToPing( source.getToPing() );
        destination.setTransferStatus( source.getTransferStatus() );
        destination.setCrawlUrl( source.getCrawlUrl() );
        System.out.println( "CopyValue of: " + source.getCrawlUrl() );
    }

    public static String getContent( String postTitle ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        Criteria crit = session.createCriteria( Posts.class );
        crit.add( Restrictions.eq( Constant.Posts_title_columnName, postTitle ) );
        List l = crit.list();
        String result = null;
        try{
            if( l == null || l.isEmpty() ){
                return result;
            }
            Posts p = (Posts) l.get( 0 );
            result = p.getContent();
        }catch( Exception ex ) {
        }
        session.close();
        
        return result;
    }
        
}
