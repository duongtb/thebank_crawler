/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.crawlerdb.pojos;

/**
 *
 * @author root
 */
public class Constant {
    
    public static final String Posts_title_columnName = "title";
    public static final String Posts_content_columnName = "content";
    public static final String Posts_crawlUrl_columnName = "crawlUrl";
    
    /**
     * Note: Field TransferStatus mean:
     *      0: Newly created.
     *      1: Saved to DiemUuDaiDB.
     *      2: Need to update.
     */
    public static final String Posts_transferStatus_columnName = "transferStatus";
    public static final int TransferStatus_NewlyCreated = 0;
    public static final int TransferStatus_Saved = 1;
    public static final int TransferStatus_NeedToUpdate = 2;
    
    public static final long Posts_defaultAuthor = 1711;
}
