/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.impl;

import core.Dispatcher;
import dao.IDB;
import dao.crawlerdb.CrawlerHU;
import dao.crawlerdb.pojos.Posts;
import dao.diemuudaidb.DiemUuDaiHU;
import dao.diemuudaidb.pojos.FinancePosts;

/**
 *
 * @author root
 */
public class DB implements IDB {

    @Override
    public int save( Object obj ) {
        if( obj == null ) {
            return 3;
        }
        if( isExist( obj ) ) {
            return 1;
        }

        Class c = obj.getClass();
        if( c.isInstance( new Posts() ) ) {
            return CrawlerHU.save( (Posts) obj );
        }else if( c.isInstance( new FinancePosts() ) ) {
            return DiemUuDaiHU.save( (FinancePosts) obj );
        }else {
            String msg = "DB.save() -> Only support Posts and FinancePosts";
            System.out.println( msg );
            return 2;
        }
    }

    @Override
    public boolean isExist( Object obj ) {
        if( obj == null ){
            throw new NullPointerException();
        }
        Class c = obj.getClass();
        if( c.isInstance( new Posts() ) ) {
            return CrawlerHU.isExist( (Posts) obj );

        }else if( c.isInstance( new FinancePosts() ) ) {
            return DiemUuDaiHU.isExist( (FinancePosts) obj );

        }else {
            String msg = "DB.isExist() -> Only support Posts and FinancePosts type.";
            System.out.println( msg );
            return false;
        }
    }

    @Override
    public int update( Object obj ) {
        if( obj == null ){
            throw  new NullPointerException();
        }
        Class c = obj.getClass();
        if( c.isInstance( new Posts() ) ) {
            return CrawlerHU.update( (Posts) obj );
        }else if( c.isInstance( new FinancePosts() ) ) {
            return DiemUuDaiHU.update( (FinancePosts) obj );
        }else {
            String msg = "DB.update() -> only support Post and FinancePosts type";
            System.out.println( msg );
            return 0;
        }
    }

    @Override
    public String getContent( String postTitle ) {
        if( postTitle == null || postTitle.equals( "" ) ){
            throw new NullPointerException();
        }
        return CrawlerHU.getContent( postTitle );
    }
}
