/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.diemuudaidb.pojos;

/**
 *
 * @author root
 */
public class Constant {
    
    public static final String FP_content_columnName = "postContent";
    public static final String FP_title_columnName = "postTitle";
    public static final String FP_postdate_columnName = "postDate";
    public static final String FP_postName_columnName = "postName";
    
    public static final int FP_tintucsukien_termTaxonomyID = 3;
    public static final int FP_tintucsukien_termOrder = 0;
    public static final long FP_default_postAuthor = 1711;
}
