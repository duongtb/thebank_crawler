/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.diemuudaidb;

import dao.diemuudaidb.pojos.Constant;
import dao.diemuudaidb.pojos.FinancePosts;
import dao.diemuudaidb.pojos.FinanceTermRelationships;
import dao.diemuudaidb.pojos.FinanceTermRelationshipsId;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * Hibernate Utility class with a convenient method to get Session Factory
 * object.
 *
 * @author root
 */
public class DiemUuDaiHU {

    private static final SessionFactory sessionFactory;
    
    static {
        try {
            // Create the SessionFactory from standard (hibernate.cfg.xml) 
            // config file.
            String xmlPath = "dao/diemuudaidb/dud.hibernate.cfg.xml";
            Configuration config = new Configuration().configure( xmlPath );
            sessionFactory = config.buildSessionFactory();
            
        }catch( Throwable ex ) {
            // Log the exception. 
            System.err.println( "Initial SessionFactory creation failed." + ex );
            throw new ExceptionInInitializerError( ex );
        }
    }
    
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    public static boolean isExist( FinancePosts financePosts ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        Criteria crit = session.createCriteria( financePosts.getClass() );
        crit.add( Restrictions.eq( Constant.FP_content_columnName, financePosts.getPostName() ) ); 
        List l = crit.list();
        session.close();
        if( l.isEmpty() ){
            return false;
        }
        return true;
    }

    public static int save( FinancePosts financePosts ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        session.save( financePosts );
//        session.flush();
        completeRelations( session, financePosts );
        session.getTransaction().commit();
        session.close();
        return 0;
    }

    public static int update( FinancePosts financePosts ) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        
        Criteria crit = session.createCriteria( financePosts.getClass() );
        crit.add( Restrictions.eq( Constant.FP_title_columnName, financePosts.getPostTitle() ) );
        List l = crit.list();
        for( int i = 0; i < l.size(); i++ ){
            FinancePosts fp = (FinancePosts) l.get( i );
            copyValue( financePosts, fp );
            session.update( fp );
        }
        session.flush();
        session.close();
        return 0;
    }
    
    private static void copyValue( FinancePosts source, FinancePosts dest ){
        if( source == null || dest == null ){
            return;
        }
        dest.setCommentCount( source.getCommentCount() );
        dest.setCommentStatus( source.getCommentStatus() );
        dest.setGuid( source.getGuid() );
        dest.setMenuOrder(  source.getMenuOrder() );
        dest.setPingStatus( source.getPingStatus() );
        dest.setPinged( source.getPinged() );
        dest.setPostAuthor( source.getPostAuthor() );
        dest.setPostContent( source.getPostContent() );
        dest.setPostContentFiltered( source.getPostContentFiltered() );
        dest.setPostDate( source.getPostDate() );
        dest.setPostDateGmt( source.getPostDateGmt() );
        dest.setPostExcerpt( source.getPostExcerpt() );
        dest.setPostMimeType( source.getPostMimeType() );
        dest.setPostModified( source.getPostModified() );
        dest.setPostModifiedGmt( source.getPostModifiedGmt() );
        dest.setPostName( source.getPostName() );
        dest.setPostParent( source.getPostParent() );
        dest.setPostPassword( source.getPostPassword() );
        dest.setPostStatus( source.getPostStatus() );
        dest.setPostTitle( source.getPostTitle() );
        dest.setPostType( source.getPostType() );
        dest.setToPing( source.getToPing() );
        
    }

    private static void completeRelations( Session session, FinancePosts financePosts ) {
        String title = financePosts.getPostTitle();
        Criteria crit = session.createCriteria( FinancePosts.class );
        crit.add( Restrictions.eq( Constant.FP_title_columnName, title ) );
        crit.addOrder( Order.desc( Constant.FP_postdate_columnName ) );
        List l = crit.list();
        try{
            FinancePosts fp = (FinancePosts) l.get( 0 );
            long fpid = fp.getId();
            FinanceTermRelationshipsId rlshp = new FinanceTermRelationshipsId( fpid, Constant.FP_tintucsukien_termTaxonomyID );
            FinanceTermRelationships termRlsh = new FinanceTermRelationships( rlshp, Constant.FP_tintucsukien_termOrder );
            
            System.out.println( "TermRelationship: " + termRlsh );
//            saveTermRelationship( termRlsh );
            session.save( termRlsh );
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
//        session.beginTransaction().commit();
//        session.flush();
    }

    private static void saveTermRelationship( FinanceTermRelationships termRlsh ) {
        Session sess = sessionFactory.openSession();
        sess.beginTransaction();
        sess.save( termRlsh );
        sess.getTransaction().commit();
        sess.close();
    }
    
    public static void main( String[] args ) {
        FinanceTermRelationshipsId id = new FinanceTermRelationshipsId( 5094, 3 );
        FinanceTermRelationships relation = new FinanceTermRelationships( id, 0 );
        
        Session sess = sessionFactory.openSession();
        sess.beginTransaction(); 
        sess.save( relation );
        sess.getTransaction().commit();
        sess.close();
        System.out.println( "Done!" );
    }
}
