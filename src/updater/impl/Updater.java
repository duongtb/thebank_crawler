/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package updater.impl;

import dao.crawlerdb.CrawlerHU;
import dao.crawlerdb.pojos.Posts;
import dao.diemuudaidb.DiemUuDaiHU;
import dao.diemuudaidb.pojos.FinancePosts;
import dao.diemuudaidb.pojos.FinanceTermRelationships;
import dao.diemuudaidb.pojos.FinanceTermRelationshipsId;
import java.io.File;
import java.util.LinkedList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import updater.IUpdater;
import util.Constant;
import util.Util;

/**
 *
 * @author root
 */
public class Updater implements IUpdater{

    @Override
    public void update(){
        /*
         * Update DiemUuDaiDB
         */
        System.out.println( "Getting Posts from local DB......................" );
        List<Posts> l = getNewPostFromCrawlerDB();
        System.out.println( "Updating Posts to DiemUuDaiDB. Size = " + l.size() + " ............. ");
        updateToDiemUuDaiDB( l );
        System.out.println( "Complete relationships in DiemUuDaiDB........." );
        completeRelationship( l );
        System.out.println( "Update status for Posts in local DB............" );
        updateTransferStatus( l );
        
        /*
         * Update imgs
         */
        System.out.println( "Uploading imgs..............." );
        updateImgs();
        
    }
    
    @Override
    public List<Posts> getNewPostFromCrawlerDB() {
        Session sess = CrawlerHU.getSessionFactory().openSession();
        sess.beginTransaction();
        
        Criteria crit = sess.createCriteria( Posts.class );
        crit.add( Restrictions.eq( dao.crawlerdb.pojos.Constant.Posts_transferStatus_columnName, 0 ) );
        List result = crit.list();
        sess.close();
        
        return result;
    }

    @Override
    public void updateToDiemUuDaiDB( List<Posts> l ) {
        Session sess = DiemUuDaiHU.getSessionFactory().openSession();
        sess.beginTransaction();
        
        for( int i = 0; i < l.size(); i++ ){
            Posts p = l.get( i );
            FinancePosts fp = cast( p );
            sess.save( fp );
        }
        
        sess.getTransaction().commit();
        sess.close();
    }

    @Override
    public void updateImgs() {
        try{
            File f = new File( Constant.LocalDirSavePath );
            if( !f.isDirectory() ){
                String msg = "Error config Util.Constant.LocalDirSavePath: " + Constant.LocalDirSavePath;
                System.out.println( msg );
                return;
            }
            
            File[] fArr = f.listFiles();
            String fileName;
            LinkedList<String> llFilePath = new LinkedList<String>();
            for( int i = 0; i < fArr.length; i++ ){
                File curFile = fArr[i];
                fileName = curFile.getName();
                if( fileName.startsWith( Constant.ImgNotTransfered ) ){
                    llFilePath.add( curFile.getPath() );
                }
            }
            Util.uploadFileToServer( llFilePath );
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        update();
    }

    private FinancePosts cast( Posts p ) {
        if( p == null ){
            System.out.println( "p null" );
            return null;
        }
        FinancePosts fp = new FinancePosts();
        try{
            fp.setCommentCount( p.getCommentCount() );
            fp.setCommentStatus( p.getCommentStatus() );
            fp.setGuid( p.getGuid() );
            fp.setMenuOrder( p.getMenuOrder() );
            fp.setPingStatus( p.getPingStatus() ); //fp.setPingStatus( "" );
            fp.setPinged( p.getPinged() );
            fp.setPostAuthor( p.getAuthor() );
            fp.setPostContent( p.getContent() );
            fp.setPostDate( p.getDate() );
            fp.setPostDateGmt( p.getDateGmt() );
            fp.setPostExcerpt( p.getExcerpt() ); //fp.setPostExcerpt( "" );
            fp.setPostMimeType( p.getPostMimeType() );
            fp.setPostModified( p.getPostModified() );
            fp.setPostModifiedGmt( p.getPostModifiedGmt() );
            fp.setPostName( p.getPostName() );
            fp.setPostParent( p.getPostParent() );
            fp.setPostPassword( p.getPostPassword() );
            fp.setPostStatus( p.getStatus() ); 
            fp.setPostTitle( p.getTitle() );
            fp.setPostType( p.getPostType() );
            fp.setToPing( p.getToPing() );
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
        return fp;
    }

    private void updateTransferStatus( List<Posts> l ) {
        Session sess = CrawlerHU.getSessionFactory().openSession();
        sess.beginTransaction();
        
        for( int i = 0; i < l.size(); i++ ){
            Posts p = l.get( i );
            p.setTransferStatus( dao.crawlerdb.pojos.Constant.TransferStatus_Saved );
            sess.update( p );
        }
        
        sess.getTransaction().commit();
        sess.close();
    }

    public static void main( String[] args ) {
        Updater up = new Updater();
        Thread t = new Thread( up );
        t.start();
    }

    private void completeRelationship( List<Posts> l ) {
        for( int i = 0; i < l.size(); i++ ){
            Posts p = l.get( i );
            String postName = p.getPostName();
            Session session = DiemUuDaiHU.getSessionFactory().openSession();
            session.beginTransaction();

            Criteria crit = session.createCriteria( FinancePosts.class );
            crit.add(  Restrictions.eq( dao.diemuudaidb.pojos.Constant.FP_postName_columnName, postName ) );
            List lt = crit.list();
            if( !lt.isEmpty() ){
                FinancePosts fp = (FinancePosts) lt.get( 0 );
                long id = fp.getId();
                Session sess = DiemUuDaiHU.getSessionFactory().openSession();
                sess.beginTransaction();

                FinanceTermRelationshipsId fti = new FinanceTermRelationshipsId( id, dao.diemuudaidb.pojos.Constant.FP_tintucsukien_termTaxonomyID );
                FinanceTermRelationships ft = new FinanceTermRelationships( fti, dao.diemuudaidb.pojos.Constant.FP_tintucsukien_termOrder );
                sess.save( ft );
                String msg = "Complete relationship of: " + postName;
                System.out.println( msg );
                
                sess.getTransaction().commit();
                sess.close();
            }
            
            session.close();
        }
    }
    
}
