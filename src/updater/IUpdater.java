/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package updater;

import dao.crawlerdb.pojos.Posts;
import java.util.List;

/**
 *
 * @author root
 */
public interface IUpdater extends Runnable{

    public void update();
    
    public List<Posts> getNewPostFromCrawlerDB();
    
    public void updateToDiemUuDaiDB( List<Posts> l );
    
    public void updateImgs();
}
