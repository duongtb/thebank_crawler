/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.LinkedList;
import java.util.Properties;
import org.apache.commons.net.ftp.FTPClient;
import sun.net.TelnetOutputStream;
import sun.net.ftp.FtpClient;

/**
 *
 * @author root
 */
public class Util {

    /**
     *
     * @param url
     * @return standard url to insert into html doc.
     */
    public static String downloadImg( String url ) {
//        String fileName = genFileName();
        String fileName = genFileName( url );
        if( fileName.startsWith( Constant.ImgTransfered ) ){
            fileName = fileName.substring( Constant.ImgTransfered.length() );
            fileName = Constant.ImgNotTransfered + fileName;
            return Constant.SiteDomain + Constant.DirSavePath + fileName;
        }
        String filePath = Constant.LocalDirSavePath + fileName;
        try {
            URL uu = new URL( url );
            HttpURLConnection con = (HttpURLConnection) uu.openConnection();
            BufferedInputStream bufIn = new BufferedInputStream( con.getInputStream() );

            FileOutputStream fos = new FileOutputStream( filePath );
            BufferedOutputStream bufOut = new BufferedOutputStream( fos );

            final int SpoonSize = 1;
            byte[] spoon = new byte[SpoonSize];
            while( (bufIn.read( spoon )) > 0 ) {
                bufOut.write( spoon );
            }
            bufOut.flush();

            con.disconnect();
            fos.close();
            bufOut.close();
            bufIn.close();

        }catch( Exception ex ) {
            ex.printStackTrace();
        }

        return Constant.DirSavePath + fileName;
    }

    private static String genFileName() {
        long cur = System.currentTimeMillis();
        return Constant.ImgNotTransfered + cur;
    }

    private static String genFileName( String url ){
        String fileName;
        try{
            String[] eles = url.split( "/" );
            fileName = eles[ eles.length - 1 ];
            
            File f = new File( Constant.LocalDirSavePath );
            File[] ff = f.listFiles();
            String tempName = Constant.ImgTransfered + fileName;
            for( int i = 0; i < ff.length; i++ ){
                String curName = ff[i].getName();
                if( curName.equals( tempName ) ){
                    return tempName;
                }
            }
            
        } catch( Exception ex ) {
            fileName = "Error in getting file name. URL = " + url;
            ex.printStackTrace();
        }
        return Constant.ImgNotTransfered + fileName;
    }
    
    public static void uploadFileToServer( LinkedList<String> llFilePath ){
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession( Constant.Username, Constant.ServerName, Constant.Port );
            session.setPassword( Constant.Password );
            Properties config = new Properties();
            config.put( "StrictHostKeyChecking", "no" );
            session.setConfig( config );
            session.connect();
            Channel chan = session.openChannel( "sftp" );
            chan.connect();

            ChannelSftp sftp = (ChannelSftp) chan;
            sftp.cd( Constant.DirWordpressRoot );
            sftp.cd( Constant.DirSavePath );

            for( int i = 0; i < llFilePath.size(); i++ ){
                File f = new File( llFilePath.get( i ) );
                InputStream is = new FileInputStream( f );
                String fileName = f.getName();
                sftp.put( is, fileName );
                
                String msg = "Uploading to server: " + f.getPath();
                System.out.println( msg );
                
                //Update fileName
                if( fileName.startsWith( Constant.ImgNotTransfered ) ){
                    fileName = fileName.substring( Constant.ImgNotTransfered.length() );
                    fileName = Constant.ImgTransfered + fileName;
                }
                File updatedFile = new File( Constant.LocalDirSavePath + fileName );
                f.renameTo( updatedFile );
            }
            sftp.disconnect();

        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }
    
    public static void uploadFileToServer( String filePath ) {
        try {
            JSch jsch = new JSch();
            Session session = jsch.getSession( Constant.Username, Constant.ServerName, Constant.Port );
            session.setPassword( Constant.Password );
            Properties config = new Properties();
            config.put( "StrictHostKeyChecking", "no" );
            session.setConfig( config );
            session.connect();
            Channel chan = session.openChannel( "sftp" );
            chan.connect();

            ChannelSftp sftp = (ChannelSftp) chan;
            sftp.cd( Constant.DirWordpressRoot );
            sftp.cd( Constant.DirSavePath );

            File f = new File( filePath );
            InputStream is = new FileInputStream( filePath );
            sftp.put( is, f.getName() );
            sftp.disconnect();

        }catch( Exception ex ) {
            ex.printStackTrace();
        }

    }

    public static void uploadFileToServer2( String filePath ) {
        try {
            FTPClient client = new FTPClient();
            client.connect( Constant.ServerName, Constant.Port );
            client.login( Constant.Username, Constant.Password );

            InputStream is = new FileInputStream( filePath );
            client.storeFile( "hehe", is );

            client.disconnect();
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
    }

//    public static void uploadFileToServer3( String filePath ) {
//        try {
//            FtpClient client = new FtpClient();
//            client.openServer( Constant.ServerName, Constant.Port );
//
//            client.login( Constant.Username, Constant.Password );
//            client.cd( Constant.DirWordpressRoot );
//            client.cd( Constant.DirSavePath );
//            client.binary();
//
//            TelnetOutputStream telOut = client.put( filePath ); //fileName
//            final int SpoonSize = 16;
//            byte[] spoon = new byte[SpoonSize];
//
//            FileInputStream fis = new FileInputStream( filePath );
//            BufferedInputStream bufIn = new BufferedInputStream( fis );
//
//            while( (bufIn.read( spoon )) > 0 ) {
//                telOut.write( spoon );
//                System.out.println( "Write out: " + spoon );
//            }
//            telOut.flush();
//
//            telOut.close();
//            fis.close();
//            bufIn.close();
//
//        }catch( Exception ex ) {
//            ex.printStackTrace();
//        }
//        System.out.println( "Done." );
//    }

    public static void main( String[] args ) {
//        String url = "http://pico.vn/Uploads/Markets/331032013/index_01.jpg";
//        String surl = downloadImg( url );
//        System.out.println( surl );

        String filePath = "/imgs/0.crawler1";
//        uploadFileToServer( filePath );
        File f1 = new File( filePath );
        File f = new File( "/imgs/dcm" );
        f1.renameTo( f );
    }
}
