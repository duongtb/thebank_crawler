/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.util;

import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
/**
 *
 * @author Tran Duong
 */
public class Utility {

    public static void RemoveStyle(Element programElement) {
        Elements allChild = programElement.getAllElements();
        allChild.removeAttr("style");
    }
    
}
