/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector;

import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
/**
 *
 * @author root
 */
public abstract class Collector implements Runnable{
    static LinkedList<ICollectorListener> Listeners;
    public static void setListeners( LinkedList<ICollectorListener> listeners ){
        Listeners = listeners;
    }
    
    public void notify_postCollected( Posts post ){
        for( int i = 0; i < Listeners.size(); i++ ){
            Listeners.get( i ).receivePost( post );
        }
    }
}
