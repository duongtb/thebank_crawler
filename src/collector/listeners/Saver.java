/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.listeners;

import collector.ICollectorListener;
import core.Core;
import dao.crawlerdb.pojos.Constant;
import dao.crawlerdb.pojos.Posts;

/**
 *
 * @author root
 */
public class Saver implements ICollectorListener{

    @Override
    public int receivePost( Posts post ) {
        Filter.fil( post );
        int result = Core.DB.save( post );
        if( result == 1 ){
            System.out.println( "Post duplicated " + post);
            System.out.println( "Now, we do NOT update the recrawled URL." );
//            System.out.println( "Updating it......" );
//            post.setTransferStatus( Constant.TransferStatus_NeedToUpdate );
//            Core.DB.update( post );
        }else{
            System.out.println( "Save post: " + post.getTitle() + ", from: " + post.getCrawlUrl());
        }
        return result;
    }
    
}
