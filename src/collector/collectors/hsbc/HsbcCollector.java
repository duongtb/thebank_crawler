/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.hsbc;

import collector.Collector;
import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.Util;
//import java.lang.Exception;
//import java.net.URLEncoder;
//import org.jsoup.safety.Whitelist;
import java.util.Hashtable;
import collector.util.Utility;
/**
 *
 * @author root
 */
public class HsbcCollector extends Collector{
    
    String CauHoiThuongGapURL = "http://www.hsbc.com.vn/1/2/personal/trung_tam_dich_vu_khach_hang/nhung_cau_hoi_thuong_gap";
    String PromotionURL = "http://www.hsbc.com.vn/1/2/personal/promotion";
    Hashtable<String,Integer> crawledLink = new Hashtable<String,Integer>();
    @Override
    public void run() {
        // Collect Question and Answer Data
        collectQAPost( CauHoiThuongGapURL );
        
        try
        {
            // Collect Promotion Data
            // Get Detail Promotion Link
            LinkedList<String> llPromotionLinks = getDetailPromotionLink(PromotionURL);
            for (String link : llPromotionLinks)
            {
                if ( crawledLink.contains(link) == false)
                {
                    // Extract Detail Data of a Promotion Post
                    extractDetailPromotion(link);
                    //Thread.sleep(1000);
                    
                    crawledLink.put(link, 0);
                }
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();            
        }
    }

    private void extractDetailPromotion(String link)
    {
        try
        {
            Document doc = Jsoup.connect( link )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10*1000).get();
            
            // Get Program Title
            String query = "h1";
            Elements h1Arr = doc.select( query );
            h1Arr.select("a").unwrap();
            if ( h1Arr.size() > 0 )
            {
                Element h1 = h1Arr.get(0);
                Posts p = new Posts();
                p.setTitle(h1.text());

                Element programTable = h1.nextElementSibling();            

                if ( programTable != null)
                {
                    try
                    {
                        //extract imgs
                        Elements imgs =  programTable.select( "img" );
                        for( int i  = 0; i < imgs.size(); i++ ){
                            Element img = imgs.get( i ); 
                            String srcLink = img.absUrl( "src" );
                            String strLinkTmp = srcLink;//URLEncoder.encode(srcLink, "UTF-8");
                            String ref = Util.downloadImg( strLinkTmp );
                            img.attr( "src", ref ); 
                        }                    
                    }
                    catch(Exception ex)
                    {
                        ex.printStackTrace();
                    }
                    //String content = Jsoup.clean(programTable.html(), Whitelist.basic());
                    programTable.select("a").unwrap();
                    Utility.RemoveStyle(programTable); // DuongTB May 06 - remove style on crawled data before save to content
                    String content = programTable.html();
                    p.setCrawlUrl(link);
                    p.setContent(content);
                    super.notify_postCollected( p );
                    System.out.println("Completed Link : " + link);
                }
                else
                {
                    // Cac noi dung chua tach duoc
                    System.out.println("Khong co dang table : " + link);
                }
            }
            else
            {
                // Cac noi dung chua tach duoc
                System.out.println("Khong co the h1 : " + link);                
            }
        }
        catch (Exception ex)
        {
            System.out.println("Link error is : " + link);
            ex.printStackTrace();
        }
        return;
    }
    private LinkedList<String> getDetailPromotionLink(String strPromotionURL) throws Exception
    {
        LinkedList<String> llLinks = new LinkedList<String>();
        try
        {
            Document doc = Jsoup.connect( strPromotionURL )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10*1000).get();
            String query = "div[class=hsbcContentStyle08text]";
            Elements divs = doc.select( query );
            for ( Element el : divs )
            {
                Element aTag = el.select("a").get(0);
                String strLinkTmp = aTag.absUrl("href");// URLEncoder.encode(aTag.absUrl("href"), "UTF-8");
                llLinks.add(strLinkTmp);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Get Detail Promotion Failed!",ex);
        }
        return llLinks;
    }
    
    private void collectQAPost( String QandA_URL ) {
        try{
            Document doc = Jsoup.connect( QandA_URL )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10*1000).get();
            String query = "a[id*=ques_]";
            Elements as = doc.select( query );
            for( int i = 0; i < as.size(); i++ ){
                String ques_title = as.get(i).text();
                String ans_id = as.get(i).id();
                ans_id = ans_id.replace("ques_","ans_");
                Elements els = doc.select("div[id=" + ans_id + "]");
                els.select("a").unwrap();
                if ( els.size() > 0 && els.size() < 2 )
                {                    
                    //String ans_content = Jsoup.clean(els.get(0).html(), Whitelist.basic());
                    String ans_content = els.get(0).html();
                    // If there is no exception, create a Post
                    Posts p = new Posts();
                    p.setTitle( ques_title );
                    p.setContent( ans_content );
                    p.setCrawlUrl(QandA_URL);
                    super.notify_postCollected( p );
                }
                else if ( els.size() >= 2 )
                {
                    throw new Exception("There are more than 1 elements which has id = " + ans_id);
                }
                
            }
        }catch( Exception ex ) {
            ex.printStackTrace();
        }
        return;
    }   
    
}
