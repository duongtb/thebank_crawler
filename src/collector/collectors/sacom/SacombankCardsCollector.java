/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.sacom;

import collector.Collector;
import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.Util;
//import java.lang.Exception;
//import java.net.URLEncoder;
//import org.jsoup.safety.Whitelist;
import java.util.Hashtable;
import collector.util.Utility;
/**
 *
 * @author root
 */
public class SacombankCardsCollector extends Collector{
    String CardNewURL = "http://www.sacombank.com.vn/canhan/Pages/The.aspx";
    Hashtable<String, Integer> crawledLink = new Hashtable<String,Integer>();
    @Override
    public void run() {
        try
        {
            // Collect News Data
            // Get Detail News Link
            LinkedList<String> llNewLinks = getNewsLink(CardNewURL);
            for (String link : llNewLinks)
            {
                if ( crawledLink.contains(link) == false )
                {
                    // Extract Detail Data of a News Post
                    extractDetailNews(link);
                    //Thread.sleep(10000);
                    
                    crawledLink.put(link, 0);
                }
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();            
        }
    }

    private void extractDetailNews(String link)
    {
        try
        {
            Document doc = Jsoup.connect( link )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10*1000).get();
            
            // Get Program Title
            String query = "div[class=title]";
            Elements titleArr = doc.select( query );
            titleArr.select("a").unwrap();
            if ( titleArr.size() > 0 )
            {
                Element title = titleArr.get(0);
                Posts p = new Posts();
                p.setTitle(title.text());

                Elements divArticles = doc.select("div[id=tbti]");    
                if ( divArticles.size() > 0 )
                {
                    Element divArticle = divArticles.get(0);
                    if ( divArticle != null)
                    {
                        try
                        {
                            //extract imgs
                            Elements imgs =  divArticle.select( "img" );
                            for( int i  = 0; i < imgs.size(); i++ ){
                                Element img = imgs.get( i ); 
                                String srcLink = img.absUrl( "src" );
                                String strLinkTmp = srcLink.replace(" ", "%20");//URLEncoder.encode(srcLink, "UTF-8");
                                String ref = Util.downloadImg( strLinkTmp );
                                img.attr( "src", ref ); 
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }

                        //String content = Jsoup.clean(programTable.html(), Whitelist.basic());
                        divArticle.select("a").unwrap();
                        Utility.RemoveStyle(divArticle); // DuongTB May 06 - remove style on crawled data before save to content
                        String content = divArticle.html();
                        p.setCrawlUrl(link);
                        p.setContent(content);
                        super.notify_postCollected( p );
                        System.out.println("Completed Link : " + link);
                    }
                    else
                    {
                        // Cac noi dung chua tach duoc
                        System.out.println("Khong the article-ds : " + link);
                    }
                }
            }
            else
            {
                // Cac noi dung chua tach duoc
                System.out.println("Khong co the div.title : " + link);                
            }
        }
        catch (Exception ex)
        {
            System.out.println("Link error is : " + link);
            ex.printStackTrace();
        }
        return;
    }
    
    private LinkedList<String> getNewsLink(String strNewsURL) throws Exception
    {
        LinkedList<String> llLinks = new LinkedList<String>();
        try
        {
            Document doc = Jsoup.connect( strNewsURL )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .timeout(10*1000).get();
            String query = "div[class=tinsp-item-tt]";
            Elements divs = doc.select( query );
            String strLinkTmp = "";
            for ( Element el : divs )
            {
                Element aTag = el.select("a").get(0);
                strLinkTmp = aTag.absUrl("href");//URLEncoder.encode(aTag.absUrl("href"), "UTF-8");
                llLinks.add(strLinkTmp);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Get Detail News Failed!",ex);
        }
        return llLinks;
    }
    
}
