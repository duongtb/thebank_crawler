/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.acb;

/**
 *
 * @author Tran Duong
 */
public class ACBCustomerLinkData {
    private String link;
    private String title;

    /**
     * @return the link
     */
    public String getLink() {
        return link;
    }

    /**
     * @param link the link to set
     */
    public void setLink(String link) {
        this.link = link;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
