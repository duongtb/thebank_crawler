/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.acb;

import collector.Collector;
import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.Util;
//import java.lang.Exception;
//import java.net.URLEncoder;
//import org.jsoup.safety.Whitelist;
import java.util.Hashtable;
import collector.util.Utility;
/**
 *
 * @author root
 */
public class ACBCardNewsCollector extends Collector{
    String[] arrTinTucURL = { "http://acbcard.com.vn/index.php/tin-tuc-su-kien"
                            };
    LinkedList<String> llNewLinks = new LinkedList<String>();
    Hashtable<String,Integer> crawledLink = new Hashtable<String,Integer>();
    @Override
    public void run() {
        try
        {
            for (String topTinTucLink : arrTinTucURL)
            {
                LinkedList<String> llPageLinks = getPageLink(topTinTucLink);

                for ( String pageLink : llPageLinks )
                {
                    // Collect News Data
                    // Get Detail News Link
                    llNewLinks = getNewsLink(pageLink);
                    while ( !llNewLinks.isEmpty() )
                    {
                        String link = llNewLinks.pop();
                        if ( crawledLink.contains(link) ==  false )
                        {
                            // Extract Detail Data of a News Post
                            extractDetailNews(link);
                            //Thread.sleep(10000);
                            crawledLink.put(link, 0);
                        }
                    }
                }
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();            
        }
    }

    private LinkedList<String> getPageLink(String link) throws Exception
    {
        LinkedList<String> llPages = new LinkedList<String>();
        // Lay page list
        // div[class=pd-newscm]
        Document doc = Jsoup.connect( link )
                        .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                        .timeout(10*1000).get();
        Elements pager = doc.select(":matchesOwn(Trang [0-9]+ của [0-9]+)");
        if ( pager.size() > 0 )
        {
            Element pagerText = pager.get(0);            
            String pText = pagerText.text();
            String[] arrText = pText.split(" ");
            Integer maxPage = new Integer(arrText[3]);
            if ( maxPage > 1)
            {
                String strLinkTmp = "";
                for ( int i = 1; i <= maxPage; i++ )
                {
                    strLinkTmp = link + "?start=" + ( 4*i - 4);
                    llPages.add(strLinkTmp);
                }
            }
        }
        return llPages;
    }
    private void extractDetailNews(String link)
    {
        try
        {
            Document doc = Jsoup.connect( link )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .followRedirects(true)
                            .timeout(30*1000).get();
            
            // Get Program Title
            String query = "td[class=contentheading]";
            Elements titleArr = doc.select( query );
            if ( titleArr.size() > 0 )
            {
                Element title = titleArr.get(0);
                Posts p = new Posts();
                p.setTitle(title.text());

                Elements divArticles = doc.select("table[class=contentpaneopen]");    
                if ( divArticles.size() > 1 )
                {
                    Element divArticle = divArticles.get(1);
                    if ( divArticle != null)
                    {
                        try
                        {
                            //extract imgs
                            Elements imgs =  divArticle.select( "img" );
                            for( int i  = 0; i < imgs.size(); i++ ){
                                Element img = imgs.get( i ); 
                                String srcLink = img.absUrl( "src" );
                                String strLinkTmp = srcLink.replace(" ", "%20");//URLEncoder.encode(srcLink, "UTF-8");
                                String ref = Util.downloadImg( strLinkTmp );
                                img.attr( "src", ref ); 
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                        // Remove old news items
                        Elements horizonLines = divArticle.select("hr[color=maroon]");
                        if ( horizonLines.size() > 0 )
                        {
                            // Extract Related Item Part to get links
                            Element relItems = horizonLines.get(0).parent();
                            
                            // Select all item a to get href to put into news list
                            Elements relLinks = relItems.select("a");
                            String strLinkTmp = "";
                            for ( Element itemLink : relLinks)
                            {
                                strLinkTmp = itemLink.absUrl("href");//URLEncoder.encode(aTag.absUrl("href"), "ISO-8859-1");//
                                /*
                                 * DuongTB comment out due to 404 error by %2F phenomenon on the link
                                 */
//                                strLinkTmp = strLinkTmp.replace("http://acbcard.com.vn/index.php/", "");
//                                strLinkTmp = URLEncoder.encode(strLinkTmp,"UTF-8");
//                                strLinkTmp = "http://acbcard.com.vn/index.php/" + strLinkTmp;
                                llNewLinks.push(strLinkTmp);
                                System.out.println("Put to link list : " + strLinkTmp);
                            }
                            // Remove the related item part
                            horizonLines.get(0).parent().remove();
                        }
                        //String content = Jsoup.clean(programTable.html(), Whitelist.basic());
                        divArticle.select("a").unwrap();
                        Utility.RemoveStyle(divArticle); // DuongTB May 06 - remove style on crawled data before save to content
                        String content = divArticle.html();
                        p.setCrawlUrl(link);
                        p.setContent(content);
                        super.notify_postCollected( p );
                        System.out.println("Completed Link : " + link);
                    }
                    else
                    {
                        // Cac noi dung chua tach duoc
                        System.out.println("Khong the table[class=contentpaneopen] : " + link);
                    }
                }
            }
            else
            {
                // Cac noi dung chua tach duoc
                System.out.println("Khong co the td[class=contentheading] : " + link);                
            }
        }
        catch (Exception ex)
        {
            System.out.println("Link error is : " + link);
            ex.printStackTrace();
        }
        return;
    }
    
    private LinkedList<String> getNewsLink(String strNewsURL) throws Exception
    {
        LinkedList<String> llLinks = new LinkedList<String>();
        try
        {
            Document doc = Jsoup.connect( strNewsURL )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .followRedirects(true)
                            .timeout(10*1000).get();
            String query = "table[class=read_block]";
            Elements divs = doc.select( query );
            String strLinkTmp = "";
            for ( Element el : divs )
            {
                Elements arrTag = el.select("a[class=readon]");
                if ( arrTag.size() > 0 )
                {
                    Element aTag = arrTag.get(0);
                    strLinkTmp = aTag.absUrl("href");//URLEncoder.encode(aTag.absUrl("href"), "ISO-8859-1");//
                    /*
                    * DuongTB comment out due to 404 error by %2F phenomenon on the link
                    */
//                    strLinkTmp = strLinkTmp.replace("http://acbcard.com.vn/index.php/", "");
//                    strLinkTmp = URLEncoder.encode(strLinkTmp,"UTF-8");
//                    strLinkTmp = "http://acbcard.com.vn/index.php/" + strLinkTmp;
                    llLinks.add(strLinkTmp);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Get Detail News Failed!",ex);
        }
        return llLinks;
    }
    
}
