/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.acb;

import collector.Collector;
import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.Util;
//import java.lang.Exception;
//import java.net.URLEncoder;
//import org.jsoup.safety.Whitelist;
import java.util.Hashtable;
import collector.util.Utility;
/**
 *
 * @author root
 */
public class ACBCustomerCollector extends Collector{
    String[] arrTinTucURL = { "http://www.acb.com.vn/khcn/", // customer                              
                            };
    LinkedList<ACBCustomerLinkData> llNewLinks = new LinkedList<ACBCustomerLinkData>();
    Hashtable<String, Integer> crawledLink = new Hashtable<String, Integer>();
    @Override
    public void run() {
        try
        {
            for (String topTinTucLink : arrTinTucURL)
            {                
                // Collect News Data
                // Get Detail News Link
                llNewLinks = getNewsLink(topTinTucLink);                
                while ( !llNewLinks.isEmpty() )
                {
                    ACBCustomerLinkData linkObj = llNewLinks.pop();
                    
                    if ( crawledLink.containsKey(linkObj.getLink()) == false )
                    {
                        // Extract Detail Data of a News Post
                        extractDetailNews(linkObj);
                        //Thread.sleep(10000);
                        crawledLink.put(linkObj.getLink(), 0);
                        
                        llNewLinks.addAll(getNewsLink(linkObj.getLink()));
                    }                    
                }
                
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();            
        }
    }

    
    private void extractDetailNews(ACBCustomerLinkData linkObj)
    {
        try
        {
            String link = linkObj.getLink();
            if ( link.contains("/khcn") )
            {
                Document doc = Jsoup.connect( link )
                                .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                                .followRedirects(true)
                                .timeout(30*1000).get();

                // Get Program Content
                String query = "td[colspan=5][align=left][valign=top]";
                Elements dataArr = doc.select( query );
                if ( dataArr.size() > 0 )
                {
                    Element datArticle = dataArr.get(0);
                    Posts p = new Posts();
                    p.setTitle(linkObj.getTitle());


                    if ( datArticle != null)
                    {
                        try
                        {
                            //extract imgs
                            Elements imgs =  datArticle.select( "img" );
                            for( int i  = 0; i < imgs.size(); i++ ){
                                Element img = imgs.get( i ); 
                                String srcLink = img.absUrl( "src" );
                                String strLinkTmp = srcLink.replace(" ", "%20");//URLEncoder.encode(srcLink, "UTF-8");
                                String ref = Util.downloadImg( strLinkTmp );
                                img.attr( "src", ref ); 
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                        
                        //String content = Jsoup.clean(programTable.html(), Whitelist.basic());
                        datArticle.select("a").unwrap();
                        Utility.RemoveStyle(datArticle); // DuongTB May 06 - remove style on crawled data before save to content
                        String content = datArticle.html();
                        p.setCrawlUrl(linkObj.getLink());
                        p.setContent(content);
                        super.notify_postCollected( p );
                        System.out.println("Completed Link : " + linkObj.getLink());                                                                     
                    }
                }
                else
                {
                    // Cac noi dung chua tach duoc
                    System.out.println("Khong co the html body table tbody tr[2] td[2] table tbody tr[2] td : " + linkObj.getLink());                
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Link error is : " + linkObj.getLink());
            ex.printStackTrace();
        }
        return;
    }
    
    private LinkedList<ACBCustomerLinkData> getNewsLink(String strNewsURL) throws Exception
    {
        LinkedList<ACBCustomerLinkData> llLinks = new LinkedList<ACBCustomerLinkData>();
        try
        {
            Document doc = Jsoup.connect( strNewsURL )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .followRedirects(true)
                            .timeout(10*1000).get();
            String query = "a[class=leftnav]";
            Elements divs = doc.select( query );
            String strLinkTmp = "";
            String strTitleTmp = "";
            for ( Element el : divs )
            {
                Element aTag = el;
                strLinkTmp = aTag.absUrl("href");//URLEncoder.encode(aTag.absUrl("href"), "ISO-8859-1");//
                /*
                * DuongTB comment out due to 404 error by %2F phenomenon on the link
                */
//                if ( strLinkTmp.contains("/khcn/") )
//                {
//                    strLinkTmp = strLinkTmp.replace("http://www.acb.com.vn/khcn/", "");
//                    strLinkTmp = URLEncoder.encode(strLinkTmp,"UTF-8");
//                    strLinkTmp = "http://www.acb.com.vn/khcn/" + strLinkTmp;
//                }
                ACBCustomerLinkData obj = new ACBCustomerLinkData();
                
                strTitleTmp = aTag.text();
                obj.setLink(strLinkTmp);
                obj.setTitle(strTitleTmp);               
                
                llLinks.add(obj);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Get Detail News Failed!",ex);
        }
        return llLinks;
    }
    
}
