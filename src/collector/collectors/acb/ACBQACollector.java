/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package collector.collectors.acb;

import collector.Collector;
import dao.crawlerdb.pojos.Posts;
import java.util.LinkedList;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import util.Util;
import collector.util.Utility;
//import java.lang.Exception;
//import java.net.URLEncoder;
//import org.jsoup.safety.Whitelist;
/**
 *
 * @author root
 */
public class ACBQACollector extends Collector{
    String[] arrQAURL = { "http://acbcard.com.vn/cauhoi.html"
                            };
    LinkedList<String> llNewLinks = new LinkedList<String>();
    @Override
    public void run() {
        try
        {
            for (String link : arrQAURL)
            {
                // Extract Detail Data of a News Post
                extractDetailNews(link);
                //Thread.sleep(10000);   
            }
        }
        catch ( Exception ex )
        {
            ex.printStackTrace();            
        }
    }

   
    private void extractDetailNews(String link)
    {
        try
        {
            Document doc = Jsoup.connect( link )
                            .userAgent("Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/535.21 (KHTML, like Gecko) Chrome/19.0.1042.0 Safari/535.21")
                            .followRedirects(true)
                            .timeout(30*1000).get();
            
            // Get Program Title
            String query = "a[class=cauhoi]";
            Elements quesArr = doc.select( query );
            for (Element ques : quesArr ){
                // for each question get question content and question answer.
                // question content is put into title of a news
                // answer content is put into content of a news                
                Posts p = new Posts();
                p.setTitle(ques.text());
                String clickEvent = ques.attr("onclick");
                String quesID = clickEvent.replace("showhide('","");
                quesID = quesID.replace("');", "");
                
                Elements divAnswers = doc.select("div[id="+ quesID + "]");    
                if ( divAnswers.size() > 0 )
                {
                    Element divAnswer = divAnswers.get(0);
                    if ( divAnswer != null)
                    {
                        try
                        {
                            //extract imgs
                            Elements imgs =  divAnswer.select( "img" );
                            for( int i  = 0; i < imgs.size(); i++ ){
                                Element img = imgs.get( i ); 
                                String srcLink = img.absUrl( "src" );
                                String strLinkTmp = srcLink.replace(" ", "%20");//URLEncoder.encode(srcLink, "UTF-8");
                                String ref = Util.downloadImg( strLinkTmp );
                                img.attr( "src", ref ); 
                            }
                        }
                        catch(Exception ex)
                        {
                            ex.printStackTrace();
                        }
                        
                        //String content = Jsoup.clean(programTable.html(), Whitelist.basic());
                        divAnswer.select("a").unwrap();
                        Utility.RemoveStyle(divAnswer); // DuongTB May 06 - remove style on crawled data before save to content
                        String content = divAnswer.html();
                        p.setCrawlUrl(link);
                        p.setContent(content);
                        super.notify_postCollected( p );
                        System.out.println("Completed Link : " + link);
                    }
                    else
                    {
                        // Cac noi dung chua tach duoc
                        System.out.println("Khong the div[id=\"+ quesID + \"] : " + link);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            System.out.println("Link error is : " + link);
            ex.printStackTrace();
        }
        return;
    }       
    
}
