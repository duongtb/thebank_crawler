/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.crawlerdb;

import dao.crawlerdb.pojos.Posts;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class CrawlerHUTest {
    
    public CrawlerHUTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSessionFactory method, of class CrawlerHU.
     */
    //@Test
    public void testGetSessionFactory() {
        System.out.println( "getSessionFactory" );
        SessionFactory expResult = null;
        SessionFactory result = CrawlerHU.getSessionFactory();
        assertEquals( expResult, result );
        // TODO review the generated test code and remove the default call to fail.
        fail( "The test case is a prototype." );
    }

    /**
     * Test of isExist method, of class CrawlerHU.
     */
    //@Test
    public void testIsExist() {
        System.out.println( "isExist" );
        Posts post = new Posts();
        post.setContent( "Ta buoc di duong xa thay gan1" );
        
        boolean result = CrawlerHU.isExist( post );
        System.out.println( "result = " + result );
    }

    /**
     * Test of save method, of class CrawlerHU.
     */
    //@Test
    public void testSave() {
        System.out.println( "save" );
        Posts p = new Posts();
        
        p.setTitle( "Day la post dau tien" );
        p.setContent( "Ta buoc di duong xa thay gan");
        int result = CrawlerHU.save( p );
        System.out.println( "Result = " + result );
    }

    /**
     * Test of update method, of class CrawlerHU.
     */
    @Test
    public void testUpdate() {
        System.out.println( "update" );
        Posts posts = new Posts();
        
        posts.setTitle( "Day la post dau tien");
        posts.setContent( "duong dai khong ngai buoc chan" );
        
        int result = CrawlerHU.update( posts );
        System.out.println( "result = " + result );
    }

    /**
     * Test of getContent method, of class CrawlerHU.
     */
    //@Test
    public void testGetContent() {
        System.out.println( "getContent" );
        String postTitle = "Day la post 1dau tien";
        String expResult = "Ta buoc di duong xa thay gan";
        
        String result = CrawlerHU.getContent( postTitle );
        assertEquals( expResult, result );
        // TODO review the generated test code and remove the default call to fail.
//        fail( "The test case is a prototype." );
    }
}
