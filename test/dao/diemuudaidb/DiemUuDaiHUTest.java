/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package dao.diemuudaidb;

import dao.diemuudaidb.pojos.FinancePosts;
import java.util.Date;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author root
 */
public class DiemUuDaiHUTest {
    
    public DiemUuDaiHUTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getSessionFactory method, of class DiemUuDaiHU.
     */
    //@Test
    public void testGetSessionFactory() {
        System.out.println( "getSessionFactory" );
        SessionFactory expResult = null;
        SessionFactory result = DiemUuDaiHU.getSessionFactory();
        assertEquals( expResult, result );
        // TODO review the generated test code and remove the default call to fail.
        fail( "The test case is a prototype." );
    }

    /**
     * Test of isExist method, of class DiemUuDaiHU.
     * OK
     */
//    @Test
    public void testIsExist() {
        System.out.println( "isExist" );
        FinancePosts financePosts = new FinancePosts();
        financePosts.setPostTitle( "toi yeu viet nam");
        boolean result = DiemUuDaiHU.isExist( financePosts );
        System.out.println( "Test exits Result = " + result );
    }

    /**
     * Test of save method, of class DiemUuDaiHU.
     * OK
     */
    //@Test
    public void testSave() {
        System.out.println( "save" );
        
        FinancePosts fp = new FinancePosts();
        fp.setPostTitle( "toi yeu viet nam");
        fp.setPostContent( "Nhung ngay trong tan cung dau kho tuyet vong, toi va cuoc doi da tha thu cho nhau");
        
        int result = DiemUuDaiHU.save( fp );
        
        System.out.println( "Result = " + result );
    }

    /**
     * Test of update method, of class DiemUuDaiHU.
     */
    @Test
    public void testUpdate() {
        System.out.println( "update" );
        FinancePosts financePosts = new FinancePosts();
        financePosts.setPostTitle( "toi yeu viet nam" );
        financePosts.setPostContent( "Day la ngoi nha cua nhung nguoi ban" );
        
        int result = DiemUuDaiHU.update( financePosts );
        System.out.println( "Result = " + result );
    }
}
